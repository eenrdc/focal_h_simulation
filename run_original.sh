#!/bin/bash

append()
{
    cur=$2
    nxt=$1
    if test "x$cur" = x ; then
	echo "$nxt"
    else
	echo "$cur,$nxt"
    fi
}

usage()
{
    cat <<-EOF
	Usage: $0 [OPTIONS] 

	Options are the arguments (space separated) to PionConfig.C
	EOF
}

args=
while test $# -gt 0 ; do
    case $1 in
	-h|--help)
	    usage
	    exit 0
	    ;;
	[0-9]*)
	    args=`append $1 $args`
	    ;;
	*)
	    args=`append "\"$1\"" $args`
	    ;;
    esac
    shift
done
      

root -l -b -q Load_original.C "PionConfig.C($args)"
