void Analysis() 
{
  gSystem->Load("Pion.C");
  
  Framework::Main* main = Framework::Main::Instance();
  Framework::TreeReader* treeReader = 
    new Framework::TreeReader("Hits", "hits.root");
  Framework::ArrayReader* hitReader = 
    new Framework::ArrayReader("BLOC", "Example::Hit", "BLOC", "Hits");
  Framework::ArrayReader* particleReader = 
    new Framework::ArrayReader("Gun", "TParticle", "Gun", "Hits");
  Pion::Profiler* profiler = new Pion::Profiler(20,20);
  
  main->Add(treeReader);
  main->Add(hitReader);
  main->Add(particleReader);
  main->Add(profiler);
  
  main->SetVerbose(5);
  main->SetDebug(10);  

  if (!gROOT->IsBatch()) {
    TBrowser* b = new TBrowser("b", main);
    
    cout << "\nSetup complete.\n\nUse context menu to execute analysis, "
         << "or type\n\n\tFramework::Main::Instance()->Loop();\n\n"
         << "to analyse the events"
         << endl;
  }
}