#include "Pion.C"



//Husk at for vinkel = 0.15 er detektoren 7 meter væk og ikke 2,45 meter, som for vinkel 0 og 0,5 
void PionConfig(Int_t seed=123456, 
                Int_t no=0,
                Int_t nev=-1, 
                Double_t Etrue=40, 
                Int_t PN=20, 
                Double_t angle = 0.2, //0.15 for 7 meter 
                Double_t diameter = 2.0,
                Double_t ScintDiameter = 1.0,  
                Double_t fraction = 0.5, 
                Int_t pipes = 40, 
                Double_t zlength = 275.0, 
                Int_t mat = 1, 
                Double_t zvertex = -3000, 
                Double_t zplacement = 275, 
                Double_t WolLength = 50, 
                Int_t withWol = 0
                
                ) 
{
  TString outputFilename =
    Form("hitsNEW%02d_E%05.1f_M%05d_A%05.2f_D%05.2f_SD%04.2f_PN%02d_Z%05.1f_%08d.root",
	 no, Etrue, PN, angle, diameter, ScintDiameter, pipes, 
   zlength, seed);

  Simulation::Main*       main      = new Simulation::Main("Pion", "Pion");
  Simulation::Generator*  generator = new Simulation::Generator("Gen","");
  Pion*                   pion      = new Pion(diameter, ScintDiameter, fraction, pipes, zlength, zplacement, withWol, WolLength);
  Simulation::Display*    display   = new Simulation::Display;
  Simulation::TrackSaver* stack     = new Simulation::TrackSaver;
  Simulation::TreeWriter* writer    = new Simulation::TreeWriter("Hits", 
								 outputFilename);

  // TString srand = gSystem->Getenv("RANDOM");
  gRandom->SetSeed(seed);
  
  TGeant3TGeo* mc = new TGeant3TGeo("Geant3");
  mc->SetRootGeometry();
  // mc->SetDEBU(1,10000000,1);
  // mc->Gcflag()->idebug = 1;
  // mc->SetSWIT(4,1);
  // mc->SetProcess("DCAY", 1);  // Decay on 
  // mc->SetProcess("PAIR", 1);  // Pair production on 
  // mc->SetProcess("COMP", 1);  // Compton scattering on 
  // mc->SetProcess("PHOT", 1);  // Photo-electric effect on 
  // mc->SetProcess("PFIS", 0);  // Photo-induced fisson off
  // mc->SetProcess("DRAY", 0);  // Delta ray production off 
  // mc->SetProcess("ANNI", 1);  // e+/-e- annihilation on 
  // mc->SetProcess("BREM", 1);  // Brehmsstrahlung on 
  // mc->SetProcess("MUNU", 1);  // mu-nucleon interaction on  
  // mc->SetProcess("CKOV", 1);  // Cherenkov radiation on
  // mc->SetProcess("HADR", 1);  // Hadronic interactions on (GHEISHA)
  // mc->SetProcess("LOSS", 2);  // Full energy loss on 
  // //mc->SetProcess("LOSS", 4);  // No fluctuations in energy loss on 
  // mc->SetProcess("MULS", 1);  // Multiple scattering on 
  // mc->SetProcess("RAYL", 1);  // Rayleigh interactions on 
  // mc->SetProcess("STRA", 0);  // Energy loss strangling 
  // mc->SetProcess("SYNC", 0);  // Syncrotron radiation off 
  // mc->SetCut("CUTGAM", 1e-3); // tracking gamma's            1MeV
  // mc->SetCut("CUTELE", 1e-3); // tracking e+/e-              1MeV
  // mc->SetCut("CUTNEU", 1e-3); // tracking neutral hadrons    1MeV
  // mc->SetCut("CUTHAD", 1e-3); // tracking charged hadrons    1MeV
  // mc->SetCut("CUTMUO", 1e-3); // tracking mu-/mu+            1MeV
  // mc->SetCut("BCUTE",  1e-3); // brehmsstrahlung for e+/e-   1MeV
  // mc->SetCut("BCUTM",  1e-3); // brehmsstrahlung for mu+/mu- 1MeV
  // mc->SetCut("DCUTE",  1e-3); // delta-ray for e-/e+         1MeV
  // mc->SetCut("DCUTM",  1e-3); // delta-ray for other         1MeV
  // mc->SetCut("PPCUTM", 1e-3); // e+/e- pair from mu+/mu-     1MeV
  // mc->SetCut("TOFMAX", 1e10); // Maximum time                

  // Make generator
  Simulation::Fixed* eg = new Simulation::Fixed;
  eg->SetPdg("pi-");
  eg->SetMinTheta(-90); //for frontende vælg min=0 og maks=angle=0.2, for sideways, min=-90, maks=angle=-90 
  eg->SetMaxTheta(angle); //før 0.5
  eg->SetMinPhi(0); //for frontende vælg min=0 og maks=360, for sideways, min=0, maks=0  
  eg->SetMaxPhi(0);
  eg->SetMinM(PN);
  eg->SetMaxM(PN);
  eg->SetMaxP(Etrue);
  eg->SetMinP(Etrue);
  generator->SetGenerator(eg);
  generator->SetVertex(500, 0, zvertex); //for frontende x=0, for sideways x=500 og zvertex=275 
  
  // Add to main task 
  main->Add(generator);
  main->Add(pion);
  main->Add(display);
  main->Add(stack);
  main->Add(writer);
  
  // Set flags for execution 
  main->SetVerbose(10);
  main->SetDebug(0);

  if (!gROOT->IsBatch()) {
    TBrowser* b = new TBrowser("b", "Browser");
    Printf("\nSetup complete.\n\nUse context menu to execute simulation, "
	   "or type\n\n\tSimulation::Main::Instance()->Loop();\n\n"
	   "to generate one event");
    main->GetApplication()->GetStack()->SetKeepIntermediate(kTRUE);
  }
  else 
    main->GetApplication()->GetStack()->SetKeepIntermediate(kFALSE);

  if (nev > 0)
    Simulation::Main::Instance()->Loop(nev); // 
  
}


//____________________________________________________________________
//
// EOF
//
