import matplotlib.pyplot as plt 
import numpy as np 


Out_dia_SH = np.array([2.5,3,3.5,4,4.5,5])

Vol = ((1.05*1.05)/(Out_dia_SH/2)**2)*100

print(Vol)

E_res_SH = [11.7/97.9,8.9/45.2,10.2/31.2,3.0/17.8,2.99/12.8,7.1/14.1] #7.1/14.1 eller 2.6/10.5

E_res = [4.37/89.4,9.7/49.16,8.7/27.4,3.7/17.6,3.14/12.65]

comp_SH = [64.3/34.1,24.5/20.1,13.58/13.57,7.8/9.8,5.3/7.7,4.4/6.6] 

comp = [54.0/34.39,22.6/19.3,15.5/13.96,7.5/9.7,5.45/7.3,3.9/5.7]

p = np.linspace(0,6,6)
p2 = np.linspace(0,100,6)

p1 = [1,1,1,1,1,1]

'''
plt.figure(1)
plt.plot(Out_dia_SH,comp_SH,'o',label="data")
plt.plot(p,p1,label="e/h=1")
plt.errorbar(Out_dia_SH,comp_SH,yerr=0.2,xerr=None,ls='none')
plt.xlim([2,6])
plt.xlabel('Outer Diameter [mm]')
plt.ylabel('Compensation')
plt.title('compensation criteria versus outer pipe diameter, scint dia=2 mm')
plt.legend()
plt.show()

plt.figure(2)
plt.plot(Vol,comp_SH,'o',label="data")
plt.plot(p2,p1,label="e/h=1")
plt.errorbar(Vol,comp_SH,yerr=0.2,xerr=None,ls='none')
#plt.xlim([2,6])
plt.xlabel('Volume Percentage Scint [%]')
plt.ylabel('Compensation')
plt.title('compensation criteria versus percentage scintillator, scint dia=2 mm')
plt.legend()
plt.show()

'''

plt.figure(1)
plt.plot(Vol,comp,'o',label="data")
plt.plot(p2,p1,label="e/h=1")
plt.errorbar(Vol,comp,yerr=0.2,xerr=None,ls='none')
#plt.xlim([2,6])
plt.xlabel('Volume Percentage Scint [%]')
plt.ylabel('Compensation')
plt.title('compensation criteria versus percentage scintillator, scint dia=2 mm')
plt.legend()
plt.show()

'''
plt.figure(3)
plt.plot(Out_dia_SH,E_res_SH,'o',label="data")
plt.show()
'''
