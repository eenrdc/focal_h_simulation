import matplotlib.pyplot as plt 
import numpy as np 

Esum = [open("Esum10.dat",'r'),open("Esum20.dat",'r'),open("Esum30.dat",'r'),open("Esum40.dat",'r'),open("Esum50.dat",'r'),open("EsumT.dat",'r')]

Etrue = [open("EtrueTP.dat",'r')]

Edep = [open("Edep20_4.dat",'r'),open("Edep30_4.dat",'r'),open("Edep40_4.dat",'r')]

Epar = [open("Epar20_4.dat",'r'),open("Epar30_4.dat",'r'),open("Epar40_4.dat",'r')]

EdepN = [open("Edep20_3N.dat",'r'),open("Edep30_3N.dat",'r'),open("Edep40_3N.dat",'r')]

EparN = [open("Epar20_3N.dat",'r'),open("Epar30_3N.dat",'r'),open("Epar40_3N.dat",'r')]


st1 = []

for i in range(len(Esum)):
    k = Esum[i].read().split(',')[:-1]
    st1.append(k)


print(len(st1))
#print(max(st[0]))


EM = [[],[],[],[],[],[]]

for n in range(len(st1)):
    for i in range(len(st1[0])):
        if st1[n][i]=='0': 
            continue
    #if float(st1[i])>30:
        #continue
        EM[n].append(float(st1[n][i]))


#print(sum(E[5]))

st2 = []

for i in range(len(Etrue)):
    k2 = Etrue[i].read().split(',')[:-1]
    st2.append(k2)

ET = [[],[]]

for n in range(len(st2)):
    for i in range(len(st2[0])):
        if st2[n][i]=='0': 
            continue
    #if float(st2[i])>30:
        #continue
        ET[n].append(float(st2[n][i]))



st3 = []


for i in range(len(Edep)):
    k3 = Edep[i].read().split(',')[:-1]
    st3.append(k3)

#print(st3[1])

#Husk at ED[n] godt kan have forskellige længder  

ED = [[],[],[]]

for n in range(len(st3)):
    for i in range(len(st3[n])):
        if st3[n][i]=='0': 
            continue
    
        ED[n].append(float(st3[n][i]))



#print(sum(ED[0]))
#print(sum(EM[5]))

st4 = []

for i in range(len(Epar)):
    k4 = Epar[i].read().split(',')[:-1]
    st4.append(k4)


EP = [[],[],[]]

for n in range(len(st4)):
    for i in range(len(st4[n])):
        if st4[n][i]=='0': 
            continue
    #if float(st2[i])>30:
        #continue
        EP[n].append(float(st4[n][i]))

#print(sum(EP[0]))

Emig = [8000,12000,16000]

ED_PT = [sum(ED[0]),sum(ED[1]),sum(ED[2])]

EP_PT = [sum(EP[0]),sum(EP[1]),sum(EP[2])]

print(ED_PT)
print(EP_PT)

#Plot af den samlede partikel-energi, som funktion af den samlede energi afsat i detektoren. 

#plt.figure(1)
#plt.plot(ED_PT,ED_PT,'o')
#plt.title('EP(ED)')
#plt.show()

#Plot af den energi jeg selv giver partiklerne, som funktion af den energi afsat i detektoren. 

#plt.figure(2)
#plt.plot(ED_PT,Emig,'o')
#plt.title('Emig(ED)')
#plt.show()

#print(ED[0])

#plt.hist(st3,bins=10000)
#plt.yscale('log')
#plt.show()

#print(max(E))

st5 = []

for i in range(len(EdepN)):
    k5 = EdepN[i].read().split(',')[:-1]
    st5.append(k5)


EDN = [[],[],[]]

for n in range(len(st5)):
    for i in range(len(st5[n])):
        if st5[n][i]=='0': 
            continue
    #if float(st2[i])>30:
        #continue
        EDN[n].append(float(st5[n][i]))



st6 = []

for i in range(len(EparN)):
    k6 = EparN[i].read().split(',')[:-1]
    st6.append(k6)


EPN = [[],[],[]]

for n in range(len(st6)):
    for i in range(len(st6[n])):
        if st6[n][i]=='0': 
            continue
    #if float(st2[i])>30:
        #continue
        EPN[n].append(float(st6[n][i]))


EDN_PT = [sum(EDN[0]),sum(EDN[1]),sum(EDN[2])]
EPN_PT = [sum(EPN[0]),sum(EPN[1]),sum(EPN[2])]

#print(EDN_PT)
#print(EPN_PT)

r = [1,1,1,2,2,3,4,5]
c = [1,2,3,4,5,5,5,5]
e = [23,24,25,26,27,28,29,30]







