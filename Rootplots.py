import matplotlib.pyplot as plt 
import numpy as np 

SumEA = open("SumEA.dat",'r')
SumEP = open("SumEP.dat",'r')

#print(SumEA)

EA = SumEA.read().split(',')[:-1]

EA_np = np.array(EA)

EA_npP = np.array([])

EP = SumEP.read().split(',')[:-1]

EP_np = np.array(EP)

EP_npP = np.array([])


for i in range(len(EA_np)):
    if EA_np[i]=='0':
        continue
    if EP_np[i]=='0':
        continue
    EA_npP = np.append(EA_npP,float(EA_np[i]))
    EP_npP = np.append(EP_npP,float(EP_np[i]))


'''
for i in range(len(EP)):
    if EP_np[i]=='0':
        continue
    EP_npP = np.append(EP_npP,float(EP_np[i]))

'''

#print(EP_npP)
print(len(EP_npP))
print(len(EA_npP))


plt.figure(1)
plt.plot(EA_npP,EP_npP,'o')
plt.show()

k = plt.hist((EA_npP),bins=30)

wth = k[1][2]-k[1][1]

plt.figure(2)
plt.hist(((EA_npP)/wth),bins=30)
plt.yscale('log')
plt.xscale('log')
plt.show()