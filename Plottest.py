import matplotlib.pyplot as plt 
import numpy as np 
import scipy as sp
from scipy.optimize import curve_fit
from scipy import stats

Psum = [open("PrimarySum50.dat"),open("PrimarySum100.dat"),open("PrimarySum200.dat"),open("PrimarySum300.dat"),open("PrimarySum400.dat")]

st1 = []

for i in range(len(Psum)):
    k = Psum[i].read().split(',')[:-1]
    st1.append(k)


PriSum = [[],[],[],[],[]]

for n in range(len(st1)):
    for i in range(len(st1[0])):
        if st1[n][i]=='0': 
            continue
    #if float(st1[i])>30:
        #continue
        PriSum[n].append(float(st1[n][i]))

#print(PriSum)

plt.figure(1)
plt.hist(PriSum[0],bins=100)
#plt.yscale('log')
plt.title("Hist of primary particle energy deposited")
plt.xlabel("Edep [MeV]")
plt.show()

#Makin arrays for E_true 

Et500 = [500]*20

Et50 = [50]*20
Et100 = [100]*20
Et200 = [200]*20
Et300 = [300]*20
Et400 = [400]*20

Et = Et50 + Et100 + Et200 + Et300 + Et400

Et_m = [50,100,200,300,400]

EPs = PriSum[0]+PriSum[1]+PriSum[2]+PriSum[3]+PriSum[4]

EPs_m = []

for i in range(len(st1)):
    w = sp.mean(PriSum[i])
    EPs_m.append(w)


std = []

for i in range(len(st1)):
    y = stats.tstd(PriSum[i])
    std.append(y)

print(std)    


def Roofit(x,a,b):
    return a*x + b

popt, pcov = curve_fit(Roofit, EPs, Et)

print(popt)

k = np.linspace(0,20,100)

plt.figure(2)
plt.plot(PriSum[0],Et50,'o')
plt.plot(PriSum[1],Et100,'o')
plt.plot(PriSum[2],Et200,'o')
plt.plot(PriSum[3],Et300,'o')
plt.plot(PriSum[4],Et400,'o')
plt.show()

plt.figure(3)
plt.plot(EPs_m,Et_m,'o')
plt.plot(k,Roofit(k,popt[0],popt[1]))
plt.errorbar(EPs_m,Et_m,xerr=std,yerr=None,fmt='o')
plt.show()


#print(Et500)

