void PseudoCode(chain)
{
  std::map<TParticle*,Double_t> m2e;

  for (Int_i ievent = 0; ievent < chain->GetEntries(); ievent++) {
    chain->GetEntry(ievent);
    
    // Clear mapping from mother particle to sum energy loss 
    m2e.erase();
    
    for (auto hit : hits) {
      TParticle* p = hit->Particle();
      TParticle* m = p;

      // As long as we have a mother, see get the next mother up the
      // tree.
      while (m->GetMother() != -1)
	    m = particles->At(m->GetMother());

      // m is now ultimate mother

      // Perhaps needed
      if (m2e.find(m) == m2e.end()) // First registration of m
	    m2e[m] = 0;

      // Add energy-loss to sum of energy loss ultimately from m 
      m2e[m] += hit->Eloss();
    } // End loop over hits 

    for (auto me : m2e) {
      TParticle* p        = me.first;
      Double_t   sumELoss = me.second;

      hist->Fill(p->Energy(), sumEloss); // Correlate energy and sum eloss
    } // Loop over ultimate mothers causing energy loss 
  } // End loop over events 
}