#!/bin/bash

n=$1
shift

usage()
{
    cat <<-EOF
	Usage: $0 NJOBS [OPTIONS]

	Options are arguments (save seed and filename) for PionConfig2.C    
	EOF
}

case $n in
    -h|--help)
	usage
	exit 0
	;;
    *)
	echo "Will run $n Jobs"
	;;
esac

for i in `seq 1 $n` ; do
    echo "Starting job number $i ... w/arguments $@" 
    nice ./run.sh $RANDOM $i $@ > `printf hits%02d.log $i`  2>&1 &
done

