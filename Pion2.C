#ifndef __PION2__
#define __PION2__ 
#include <simulation/Task.h>
#include <TGeoMedium.h>
#include <TGeoMaterial.h>
#include <TGeoManager.h>
#include <TGeoBBox.h>
#include <TGeoTube.h>
#include <TGeoVolume.h>
#include <TGeoMatrix.h>
#include <TVirtualMC.h>
#include <TTree.h>
#include <TClonesArray.h>
#include "HitP.h"
#include <simulation/Hit.h>
#include <iostream>
#include <TGeoPgon.h>
#include <math.h>

#define PI 3.14159265

// Define variables 

//Int_t r = 25; //antal rør pr. række 

//____________________________________________________________________
class Pion2 : public Simulation::Task
{
public:
  Pion2(Double_t diameter=2.0, Double_t ScintDiameter=1.0, Double_t fraction=0.5, Int_t pipes=25, Double_t zlength=550.0, 
  Int_t mat=1,
  Double_t zplacement = 1000,
  Int_t withWol = 0,
  Double_t WolLength = 50.0);
  
  void Initialize(Option_t* option="");
  void Register(Option_t* option="");
  void Step();
  ClassDef(Pion2,0);

  bool fall; 
  Double_t fTubeDiameter;
  Double_t fScintDiameter; 
  Double_t fScintFraction;
  Int_t fNumberPipes; 
  Double_t fzlength; 
  Int_t fmat;
  Double_t fzplacement; 
  Int_t fwithWol;
  Double_t fWolLength; 

};


//____________________________________________________________________
Pion2::Pion2(Double_t diameter, Double_t ScintDiameter, Double_t fraction, Int_t pipes, Double_t zlength, Int_t mat, 
Double_t zplacement, Int_t withWol, Double_t WolLength)
  : Simulation::Task("Pion2", "Pion2"), 
    fTubeDiameter(diameter),
    fScintDiameter(ScintDiameter),
    fScintFraction(fraction),
    fNumberPipes(pipes),
    fzlength(zlength),
    fmat(mat), 
    fzplacement(zplacement),
    fwithWol(withWol),
    fWolLength(WolLength)
    


{
  //CreateDefaultHitArray();
  fCache = new TClonesArray("HitP");

  if (fScintFraction > 1 or fScintFraction < 0)
    throw std::runtime_error("Argh!");
}

//____________________________________________________________________
void
Pion2::Initialize(Option_t* option)
{
  Simulation::Task::Initialize(option);

  // Mixture Air
  // isvol,ifield,fieldm,tmaxfd,stemax,deemax,epsil,stmin
  Double_t pAir[]     = { 0., 0., 0., 1.,  .001, 1., .001, .001 };
  TGeoMixture* matAir = new TGeoMixture("Air", 4, .00120479);
  matAir->DefineElement(0, 12.0107,  6., 0.000124);
  matAir->DefineElement(1, 14.0067,  7., 0.755267);
  matAir->DefineElement(2, 15.9994,  8., 0.231781);
  matAir->DefineElement(3, 39.948,  18., 0.012827);
  matAir->SetTransparency('0');
  TGeoMedium* medAir = new TGeoMedium("Air", 1, matAir,  pAir);
  (void)medAir;
  
  // Mixture scint
  // isvol,ifield,fieldm,tmaxfd,stemax,deemax,epsil,stmin
  Double_t pScint[] = { 1, 0, 0, 20, 1e+10, 0.0308578, 0.01, 0.0127021 };
  TGeoMixture* matScint = new TGeoMixture("Scintilator", 2, 1.032);
  matScint->DefineElement(0, 1.008, 1, 0.0774908);     // H
  matScint->DefineElement(1, 12, 6, 0.922509);         // C
  TGeoMedium* medScint= new TGeoMedium("Scinitilator", 0, matScint, pScint);
  // Associate medium with this code. 
  RegisterMedium(medScint);
  
  // Material Cubber
  // isvol,ifield,fieldm,tmaxfd,stemax,deemax,epsil,stmin
  Double_t pCu[] = { 0, 0, 0, 20, 1e+10, 0.18296, 0.005, 0.0334422 };
  TGeoMaterial* matCu = new TGeoMaterial("Cubber", 63.55,29,8.96,1.436);
  TGeoMedium*   medCu = new TGeoMedium("Cubber", 0, matCu, pCu);
  (void)medCu;

  //defining aluminium material for the hexagon shape 
  // isvol,ifield,fieldm,tmaxfd,stemax,deemax,epsil,stmin
  Double_t pAl[] = { 0, 0, 0, 20, 1e+10, 0.18296, 0.005, 0.0334422 };
  TGeoMaterial* matAl = new TGeoMaterial("Aluminum", 26.98,13,2.7,8.87510);
  TGeoMedium*   medAl = new TGeoMedium("Aluminum", 0, matAl, pAl);
  (void)medAl;

  //Material Wolfram 
  // isvol,ifield,fieldm,tmaxfd,stemax,deemax,epsil,stmin
  Double_t pWol[] = { 0, 0, 0, 20, 1e+10, 0.18296, 0.005, 0.0334422 };
  // wolfram, atomvægt, atomnummer, densitet. 
  TGeoMaterial* matWol = new TGeoMaterial("Wolfram",183.85,74,19.3);
  TGeoMedium* medWol = new TGeoMedium("Wolfram", 0, matWol, pWol);

  //Material Wolfram 
  // isvol,ifield,fieldm,tmaxfd,stemax,deemax,epsil,stmin
  Double_t pBly[] = { 0, 0, 0, 20, 1e+10, 0.18296, 0.005, 0.0334422 };
  // wolfram, atomvægt, atomnummer, densitet. 
  TGeoMaterial* matBly = new TGeoMaterial("Lead",207.2,82,11.35);
  TGeoMedium* medBly = new TGeoMedium("Lead", 0, matBly, pBly);

  // Get mediums
  TGeoMedium* air   = gGeoManager->GetMedium("Air");
  TGeoMedium* cu    = gGeoManager->GetMedium("Cubber");
  TGeoMedium* scint = gGeoManager->GetMedium("Scinitilator");
  TGeoMedium* al    = gGeoManager->GetMedium("Aluminum");
  TGeoMedium* Wol   = gGeoManager->GetMedium("Wolfram");
  TGeoMedium* bly   = gGeoManager->GetMedium("Lead");

  
  // Dimensions are half-lengths
  TGeoBBox*   caveShape  = new TGeoBBox("Cave", 6000, 6000, 6000);
  TGeoVolume* caveVol    = new TGeoVolume("Cave", caveShape, air);

  //Define wolfram box
  TGeoBBox* wolframShape = new TGeoBBox("wolfram", 30, 30, fWolLength);
  TGeoVolume* wolframVol = new TGeoVolume("wolfram", wolframShape, bly);
  
  Double_t d = fScintFraction * fTubeDiameter / 2;
  Double_t fStagger = fTubeDiameter - (2*(fTubeDiameter/2)*cos(PI/6));

  // Shape of a scinitilator slap 
  TGeoTube*   scintShape = new TGeoTube("Scintilator", 0, fScintDiameter/2, fzlength);
  TGeoVolume* scintVol   = new TGeoVolume("Scintilator", scintShape, scint);

  TGeoPgon*   scintShape2 = new TGeoPgon("Scintilator", 0.00, 360.00, 6, 2);
  TGeoVolume* scintVol2   = new TGeoVolume("Scintilator", scintShape2, scint);

  scintShape2->DefineSection(0,-fzlength,0,fScintDiameter/2);
  scintShape2->DefineSection(1,fzlength,0,fScintDiameter/2);
  
  // Shape of pipe absorber cubber
  TGeoTube*   absShape   = new TGeoTube("Absorberpipe", fScintDiameter/2, fTubeDiameter/2, fzlength);
  TGeoVolume* absVolpipe     = new TGeoVolume("Absorberpipe", absShape, cu);
  
  //defining the hexagon surrounding the pipes 
  TGeoPgon* pgon         = new TGeoPgon("absorberhexa",0.00,360.00,6,2);
  TGeoVolume* absVol     = new TGeoVolume();

  //If more than one absorber material, make the hexagon aluminium, if not, make it cubber. 

  if(fmat>1){ 
     absVol     = new TGeoVolume("Absorberhexa", pgon, al);
  }
  else {
     absVol     = new TGeoVolume("Absorberhexa", pgon, cu);
    }
 


  pgon->DefineSection(0,-fzlength,fScintDiameter/2,fTubeDiameter/2);
  pgon->DefineSection(1,fzlength,fScintDiameter/2,fTubeDiameter/2);

  TGeoVolumeAssembly* pipeVol = new TGeoVolumeAssembly("Pipe");
  pipeVol->AddNode(scintVol2,1,0);
  pipeVol->AddNode(absVol,1,0);

  //TGeoVolume* vol = gGeoManager->MakePgon("PGON",cu, 0.00,360.0,6,2);
  //TGeoPgon* pgon = (TGeoPgon*)(vol->GetShape());

  //Add pipes to hexagon 

  //absVol->AddNode(absVolpipe,1,0);
  //absVol->AddNode(scintVol,1,0);

  // Say that scintilator is active, and we'd like to make hits there
  RegisterVolume(scintVol);

  gGeoManager->SetTopVolume(caveVol);
  gGeoManager->DefaultColors();

  //caveVol->AddNode(pipeVol,1);
  
  
  // 2. argument giver noden et nummer, lav sammenhængende scint og abs og giv dem numre som copy kan finde.
  for(int i=0; i < fNumberPipes; i= i + 1) {
  for(int j = 0; j < fNumberPipes; j = j + 1){
    // Int_t copy = i+j*25;
    Int_t copy = HitP::CoordToCopy(i,j);
  caveVol->AddNode(pipeVol, copy, new TGeoTranslation(
    (0-((fNumberPipes/2.0)-(1.0/2.0))*fTubeDiameter)+i*(fTubeDiameter-fStagger), 
    (0-((fNumberPipes/2)-(1.0/2.0))*fTubeDiameter)+j*fTubeDiameter-(i%2)*(1.0/2.0)*fTubeDiameter, fzplacement));
  }
  }
  
  //Add block of Wolfram to cave 
  if(fwithWol==1){ 
  caveVol->AddNode(wolframVol, 1, new TGeoTranslation(0,0,fzplacement-fzlength-fWolLength));
   }
  //-0.2679
  

  gGeoManager->SetTopVolume(caveVol);

}
//Mark

//____________________________________________________________________
void
Pion2::Register(Option_t*)
{
  TTree* tree = 0;
  if ((tree = GetBaseTree("Hits")) && fCache) 
    fBranch = tree->Branch(GetName(), &fCache);

}

//____________________________________________________________________
void
Pion2::Step()
{
  TVirtualMC* mc = TVirtualMC::GetMC();
  Int_t copy;
  Int_t vol = mc->CurrentVolID(copy);
  if (!IsSensitive(vol)) return;

  Int_t copyOff; 
  Int_t volOffID = mc->CurrentVolOffID(1,copyOff);

  if (mc->Edep() <=0) return; 
  TClonesArray* hits = static_cast<TClonesArray*>(fCache);
  Int_t n = hits->GetEntriesFast();
  TLorentzVector v, p;
  mc->TrackPosition(v);
  mc->TrackMomentum(p);
  Int_t   nTrack = mc->GetStack()->GetCurrentTrackNumber();
  Int_t   pdg    = mc->TrackPid();
  Float_t edep   = mc->Edep();

  // Int_t row, col;
  // HitP(copyOff,row,col);
  new ((*hits)[n]) HitP(copyOff, copy, edep, nTrack, pdg, v, p);
  
  //std::cout << vol << "\t" << copy << std::endl; 

  //CreateDefaultHit();

  //lålålålålå ændring
  


}
  
#endif
//____________________________________________________________________
//
// EOF
//
