void Load_original()
{
  const char* focal = "$HOME/focal_h_simulation";
  gSystem->AddIncludePath(Form("-I%s",focal));
  gROOT->SetMacroPath(Form("%s:%s",gROOT->GetMacroPath(),focal));

  gROOT->LoadMacro("HitP.h+g");
  gROOT->LoadMacro("Pion.C+g");
}